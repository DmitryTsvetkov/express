module.exports = (sequelize, DataTypes) => {
  const Country = sequelize.define('Country', {
    country: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
    },
  });
  // eslint-disable-next-line no-unused-vars
  Country.associate = (models) => {
    // associations can be defined here
  };

  return Country;
};
