
module.exports = (sequelize, DataTypes) => {
  const City = sequelize.define('City', {
    city: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
    },
  });
  // eslint-disable-next-line no-unused-vars
  City.associate = (models) => {
    // associations can be defined here
  };

  return City;
};
