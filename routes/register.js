const express = require('express');
const db = require('../models');
const validator = require("email-validator");

const router = express.Router();

router.get('/', (req, res) => {
  if (req.user !== undefined) return res.redirect('/');
  return res.render('register', { title: 'Register', errors: {} });
});

router.post('/', async (req, res) => {
  const {
    username,
    email,
    firstname,
    lastname,
    middlename,
    password,
    confirmPassword,
    fullPhone: phoneNumber,
    country,
    city,
    osMobile,
  } = req.body;

  const errors = {};

  const user = await db.User.findOne({ where: { username } });
  if (user !== null) {
    errors.username = 'Username already exists.';
  }

  if (email === undefined || !validator.validate(email)) {
    errors.email = 'E-mail is required.';
  }

  if (password !== confirmPassword) {
    errors.confirmPassword = 'Password does not match.';
  }

  if (phoneNumber === undefined || phoneNumber === '') {
    errors.phoneNumber = 'Phone Number is required.';
  }

  if (firstname === undefined || firstname === '') {
    errors.firstname = 'First Name is required.';
  }

  if (lastname === undefined || lastname === '') {
    errors.lastname = 'Last Name is required.';
  }
  if (country === undefined || country === '') {
    errors.country = 'Country is required.';
  }
  if (city === undefined || city === '') {
    errors.city = 'City is required.';
  }

  if (Object.keys(errors).length === 0) {
    const hashed_password = db.User.generateHash(password);

    try {
      await db.User.create({
        username,
        email,
        firstname,
        lastname,
        middlename,
        password: hashed_password,
        phoneNumber,
        osMobile,
        country,
        city,
        verificationMethod: 'sms',
      });
    } catch (e) {
      res.status(500).send(e.toString());
    }

    res.redirect('/');
  } else {
    errors.wasValidated = true;
    res.status(400).render('register', { title: 'Register', errors });
  }
});

module.exports = router;
