const express = require('express');
const db = require('../models');

const router = express.Router();

router.get('/city', async (req, res) => {
  const { text } = req.query;
  if (text) {
    const data = await db.City.findAll({
      attributes: ['city'],
      where: { city: { [db.op.like]: `%${text.toLowerCase()}%` } },
      limit: 5,
    });
    const result = data.map((city) => { return city.dataValues.city })
    res.status(200).send(JSON.stringify(result));
  } else res.status(200).send('no city');
});

router.get('/country', async (req, res) => {
  const { text } = req.query;
  if (text) {
    const data = await db.Country.findAll({
      attributes: ['country'],
      where: { country: { [db.op.like]: `%${text.toLowerCase()}%` } },
      limit: 5,
    });
    const result = data.map((country) => { return country.dataValues.country })
    res.status(200).send(JSON.stringify(result));
  } else res.status(200).send('no country');
});

module.exports = router;
