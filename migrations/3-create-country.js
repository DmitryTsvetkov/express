module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('Countries', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    country: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
    },
  }),
  down: queryInterface => queryInterface.dropTable('Countries'),
};
