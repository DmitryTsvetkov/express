module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('Cities', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    city: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
    },
  }),
  down: queryInterface => queryInterface.dropTable('Cities'),
};
